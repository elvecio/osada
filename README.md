**OSADA**

Osada is a social networking server app which allows communication between ActivityPub based services and Zot/6 services using the LAMP web stack.

Protocol documentation is located here:

https://macgirvin.com/wiki/mike/Zot%2BVI/Home

